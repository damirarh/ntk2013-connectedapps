﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebService.Entities;

namespace WebService
{
    [ServiceContract]
    public interface ITodoService
    {
        [OperationContract]
        IEnumerable<TodoItem> GetTodoItems();

        [OperationContract]
        TodoItem GetTodoItem(int id);

        [OperationContract]
        TodoItem SaveTodoItem(TodoItem item);
    }
}
