﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebService.Entities;

namespace WebService
{
    public class TodoService : ITodoService
    {
        public IEnumerable<TodoItem> GetTodoItems()
        {
            using (var context = new TodoContext())
            {
                return context.TodoItems.ToList();
            }
        }

        public TodoItem GetTodoItem(int id)
        {
            using (var context = new TodoContext())
            {
                return context.TodoItems.SingleOrDefault(i => i.Id == id);
            }
        }

        public TodoItem SaveTodoItem(TodoItem item)
        {
            using (var context = new TodoContext())
            {
                var existing = context.TodoItems.FirstOrDefault(i => i.Id == item.Id);
                if (existing == null)
                {
                    context.TodoItems.Add(item);
                }
                else
                {
                    existing.Title = item.Title;
                    existing.Description = item.Description;
                    existing.DueDate = item.DueDate;
                    existing.Priority = item.Priority;
                    existing.Done = item.Done;
                    item = existing;
                }
                context.SaveChanges();
                return item;
            }
        }
    }
}
