﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using RelayCommandRt;
using RssReader.Common;
using Windows.Storage;

namespace RssReader.ViewModels
{
    public class OptionsViewModel : BindableBase
    {
        private string _newUrl;
        public string NewUrl
        {
            get
            {
                return _newUrl;
            }
            set
            {
                if (_newUrl == value)
                    return;
                _newUrl = value;
                OnPropertyChanged();
            }
        }

        public string CurrentUrl { get; set; }
        public ObservableCollection<string> Feeds { get; set; }

        public RelayCommand AddCommand { get; set; }
        public RelayCommand RemoveCommand { get; set; }

        public OptionsViewModel()
        {
            Feeds = new ObservableCollection<string>();
            NewUrl = String.Empty;
            AddCommand = new RelayCommand(_ => OnAdd());
            RemoveCommand = new RelayCommand(_ => OnRemove());
            Load();
        }

        private void OnRemove()
        {
            Feeds.Remove(CurrentUrl);
            Save();
        }

        private void OnAdd()
        {
            Feeds.Add(NewUrl);
            Save();
        }

        private void Save()
        {
            ApplicationData.Current.LocalSettings.Values["Feeds"] = String.Join("|", Feeds.ToArray());
        }

        private void Load()
        {
            object value = ApplicationData.Current.LocalSettings.Values["Feeds"];
            if (value != null)
                Feeds = new ObservableCollection<string>(ApplicationData.Current.LocalSettings.Values["Feeds"].ToString().Split('|'));
            else
                Feeds = new ObservableCollection<string>();
        }
    }

}