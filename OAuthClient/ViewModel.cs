﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using OAuthClient.Annotations;
using RelayCommandRt;
using Windows.Data.Json;
using Windows.Security.Authentication.Web;

namespace OAuthClient
{
    public class ViewModel : INotifyPropertyChanged
    {
        private List<dynamic> _calendars;

        public ViewModel()
        {
            GetCalendarsCommand = new RelayCommand(async _ => await OnGetCalendars());
        }

        private async Task OnGetCalendars()
        {
            var code = await GetAuthenticationCode();
            var token = await GetAccessToken(code);
            Calendars = await GetCalendars(token);
        }

        private async Task<string> GetAuthenticationCode()
        {
            var url = String.Format("https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&response_type=code&scope={2}",
                Keys.ClientId, "urn:ietf:wg:oauth:2.0:oob", "https://www.googleapis.com/auth/calendar.readonly");

            var requestUri = new Uri(url);
            var responseUri = new Uri("https://accounts.google.com/o/oauth2/approval?");

            var result = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.UseTitle, requestUri, responseUri);
            return Regex.Match(result.ResponseData, "Success code=(?<code>.*)").Groups["code"].Value;
        }

        private async Task<string> GetAccessToken(string authenticationCode)
        {
            var form = new Dictionary<string, string>
                {
                    {"code", authenticationCode},
                    {"client_id", Keys.ClientId},
                    {"client_secret", Keys.ClientSecret},
                    {"redirect_uri", "urn:ietf:wg:oauth:2.0:oob"},
                    {"grant_type", "authorization_code"},
                };
            var content = new FormUrlEncodedContent(form);

            var client = new HttpClient();
            var response = await client.PostAsync("https://accounts.google.com/o/oauth2/token", content);
            var json = await response.Content.ReadAsStringAsync();

            return JsonObject.Parse(json)["access_token"].GetString();
        }

        private async Task<dynamic> GetCalendars(string accessToken)
        {
            var uri = String.Format("https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token={0}", accessToken);
            var client = new HttpClient();
            var calendars = await client.GetStringAsync(uri);
            var items = JsonObject.Parse(calendars)["items"].GetArray();

            return items.Select(item => new
            {
                Title = item.GetObject()["summary"].GetString(),
                Description = item.GetObject().Keys.Contains("description") ? item.GetObject()["description"].GetString() : null,
            }).Cast<dynamic>().ToList();
        }

        public List<dynamic> Calendars
        {
            get { return _calendars; }
            set
            {
                if (Equals(value, _calendars)) return;
                _calendars = value;
                OnPropertyChanged();
            }
        }

        public ICommand GetCalendarsCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}