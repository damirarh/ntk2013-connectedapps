﻿namespace OAuthClient
{
    public static class Keys
    {
        public static string ClientId = "YOUR_CLIENT_ID";
        public static string ClientSecret = "YOUR_CLIENT_SECRET";
    }
}