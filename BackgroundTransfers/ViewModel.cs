﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using BackgroundTransfers.Annotations;
using RelayCommandRt;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;

namespace BackgroundTransfers
{
    public class ViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<DownloadInfo> _downloads;

        public ViewModel()
        {
            StartDownloadCommand = new RelayCommand(async _ => await OnStartDownload());
            Init();
        }

        private async Task OnStartDownload()
        {
            var uri = new Uri("http://s3.amazonaws.com/thetabletshow/thetabletshow_0072_lhotka.mp3");
            var file = await ApplicationData.Current.LocalFolder.CreateFileAsync("download.mp3",
                                                                           CreationCollisionOption.GenerateUniqueName);
            var downloader = new BackgroundDownloader();
            var download = downloader.CreateDownload(uri, file);
            Downloads.Add(new DownloadInfo(download, true));
        }

        private async void Init()
        {
            var downloads = await BackgroundDownloader.GetCurrentDownloadsAsync();
            Downloads = new ObservableCollection<DownloadInfo>(downloads.Select(d => new DownloadInfo(d, false)).ToList());
        }

        public RelayCommand StartDownloadCommand { get; set; }

        public ObservableCollection<DownloadInfo> Downloads
        {
            get { return _downloads; }
            set
            {
                if (Equals(value, _downloads)) return;
                _downloads = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}