﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BackgroundTransfers.Annotations;
using Windows.Networking.BackgroundTransfer;

namespace BackgroundTransfers
{
    public class DownloadInfo : INotifyPropertyChanged
    {
        public DownloadInfo(DownloadOperation download, bool start)
        {
            Download = download;
            Progress = new Progress();
            Init(start);
        }

        private async void Init(bool start)
        {
            if (start)
            {
                await Download.StartAsync().AsTask(Progress);
            }
            else
            {
                await Download.AttachAsync().AsTask(Progress);
            }
        }

        public DownloadOperation Download { get; set; }
        public Progress Progress { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}