﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BackgroundTransfers.Annotations;
using Windows.ApplicationModel.Core;
using Windows.Networking.BackgroundTransfer;
using Windows.UI.Core;

namespace BackgroundTransfers
{
    public class Progress : IProgress<DownloadOperation>, INotifyPropertyChanged
    {
        private double _percentage;

        public void Report(DownloadOperation value)
        {
            var progress = value.Progress;
            CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, 
                () => Percentage = progress.BytesReceived / (double)progress.TotalBytesToReceive * 100);
        }

        public double Percentage
        {
            get { return _percentage; }
            set
            {
                if (value.Equals(_percentage)) return;
                _percentage = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}