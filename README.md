#Connected Windows Store App Samples

This is a set of samples I showed during by session on Building Connected Apps for Windows Store at NT conference 2013. It consists of six projects:

- **RssReader** is a simple RSS reader app based on Grid App project template using `SydicationClient` API.
- **WcfClient** references a WCF web service and calls asynchronous methods on the generated proxy.
- **WebService** is a helper project implementing the web service referenced by WcfClient project.
- **OAuthClient** implements OAuth authentication with Google services and calls one of its APIs.
- **ODataClient** references StackOverflow OData feed and calls a simple query on it.
- **BackgroundTransfers** uses `BackgroundDownloader` to download a longer file and includes a progress report.

##Requirements

For the solution to compile you need the following installed:

- [WCF Data Services Tools for Windows Store Apps](http://www.microsoft.com/en-us/download/details.aspx?id=30714) (until the corresponding NuGet packages are published on NuGet, you'll need to add another package source pointing to your local disk - you can read more about it [here](http://www.damirscorner.com/WCFDataServicesToolsForWindowsStoreAppsAndNuGetPackageRestore.aspx))

NuGet package restore also [requires explicit consent from the user](http://docs.nuget.org/docs/workflows/using-nuget-without-committing-packages) to download the missing packages.