﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Services.Client;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ODataClient.Annotations;
using ODataClient.StackOverflow;

namespace ODataClient
{
    public class ViewModel : INotifyPropertyChanged
    {
        private const string ServiceRoot = "http://data.stackexchange.com/stackoverflow/atom";
        
        public ViewModel()
        {
            Init();
        }

        private async void Init()
        {
            var context = new Entities(new Uri(ServiceRoot));
            var query = (DataServiceQuery<Post>)context.Posts.Expand("Parent")
                               .Where(p => p.OwnerUserId == 197913 && p.PostTypeId == 2)
                               .OrderByDescending(p => p.CreationDate);

            var result = (await ExecuteAsync(query));

            Answers = result.Select(p => p.Parent.Title).ToList();
        }

        private List<string> _answers;
        public List<string> Answers
        {
            get { return _answers; }
            set
            {
                if (Equals(value, _answers)) return;
                _answers = value;
                OnPropertyChanged();
            }
        }

        private static async Task<IEnumerable<TResult>> ExecuteAsync<TResult>(DataServiceQuery<TResult> query)
        {
            var queryTask = Task.Factory.FromAsync(query.BeginExecute(null, null),
                (queryAsyncResult) =>
                {
                    var results = query.EndExecute(queryAsyncResult);
                    return results;
                });

            return await queryTask;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}