﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WcfClient.Annotations;
using WcfClient.Service;

namespace WcfClient
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            Init();
        }

        private async void Init()
        {
            var proxy = new TodoServiceClient();
            TodoItems = await proxy.GetTodoItemsAsync();
        }

        private IEnumerable<TodoItem> _todoItems;
        public IEnumerable<TodoItem> TodoItems
        {
            get { return _todoItems; }
            set
            {
                if (Equals(value, _todoItems)) return;
                _todoItems = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}